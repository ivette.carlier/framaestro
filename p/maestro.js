// https://framaestro.org/#[a-zA-Z0-9]{8}/NomDuProjet/0,0,400,300,(urlencoded|framaservice);&togetherjs=[a-zA-Z0-9]{10}
function randomName() {
  var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  var string_length = 10;
  var randomstring = '';
  for (var i = 0; i < string_length; i++) {
    var rnum = Math.floor(Math.random() * chars.length);
    randomstring += chars.substring(rnum, rnum + 1);
  }
  return randomstring;
}

var HASH = window.location.hash;
var maestro = HASH.replace('#','').split('/');

// Random string
var tonality = (maestro[0] != undefined) ? maestro[0] : randomName();

// Project name
var concerto = (maestro[1] != undefined) ? maestro[1] : '';

// BaseURL
var a = document.createElement('a');
    a.href = window.location.href;
var BASEURL = a.protocol+"//"+a.host+a.pathname+'#'+tonality+'/'+concerto+'/';

// All tiles
var movement = (maestro[2] != undefined) ? maestro[2].split(';') : '';

// Tiles with placement top,left,width,height,url
var tile = new Array();
for(i = 0; i < movement.length; i++) {
  tile[i] = movement[i].split(',');
}

var panels = new Array();

/**
 * Create permalink and change the hash
 * - url need to be encoded for sharing
 * - triple encoding is needed to use Framalink API
 * Hash refresh is needed on
 * - new/edit tile
 * - drag&drop
 * - resize
 * - close
 * - grid apply
 **/
function makePermalink(framalink) {

  var a = document.createElement('a');
  a.href = window.location.href;

  var newHash = encodedHash = '#'+tonality+'/'+concerto+'/';

  $('.jsPanel').each( function() {
    coord = $(this).css('top').replace('px','')+','+
           $(this).css('left').replace('px','')+','+
           $(this).children('.panel-body').css('width').replace('px','')+','+
           $(this).children('.panel-body').css('height').replace('px','')+',';
    newHash = newHash + coord + encodeURIComponent($(this).find('iframe').attr('src').replace(a.protocol+"//"+a.host+a.pathname, ''))+';';
    encodedHash = encodedHash + coord + encodeURIComponent(encodeURIComponent(encodeURIComponent($(this).find('iframe').attr('src').replace(a.protocol+"//"+a.host+a.pathname, ''))))+';';
  });

  location.hash = newHash;
  if(framalink) {
    return a.protocol+"//"+a.host+a.pathname+encodedHash;
  } else {
    return a.protocol+"//"+a.host+a.pathname+newHash;
  }

}

/**
 * Create a tile
 * with position, size, url
 * a random color
 * and domain as a title
 **/
function newTile(pTop,pLeft,sWidth,sHeight,url) {

  var panel = ['primary', 'success', 'info', 'warning', 'danger'];
  var bsClass = panel[Math.floor(Math.random()*panel.length)];

  var parser = document.createElement('a');
  parser.href = decodeURIComponent((url+'').replace(/\+/g, '%20'));

  var frameLink = parser.href;
  var panelTitle = parser.hostname.replace(/^(www|test)\./i,'').replace(/\.(com|net|org|fr|pro)$/i,'');

  var panelId = $('.jsPanel').length + 1;

  function template() {
      var template = $(jsPanel.template);
      var headerR = $('.jsPanel-hdr-r', template);
      headerR.append('<div class="jsPanel-btn-reload"><i class="glyphicon glyphicon-refresh" aria-hidden="true"></i><span class="sr-only">Actualiser</span></div><div class="jsPanel-btn-edit"><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i><span class="sr-only">Modifier</span></div>');
      return template;
  }

  panels[panelId] = f$.jsPanel({
    template: template(),
    toolbarHeader: function( hdr ){
        hdr.toolbar.addClass('bg-'+bsClass).hide();
        return '<form id="editTile-'+panelId+'" action="#"><div class="input-group input-group-sm"><input id="url-'+panelId+'" type="text" class="form-control" value="'+frameLink+'"><span class="input-group-btn"><button class="btn btn-default" type="submit">Ok</button></span></div></form>';
    },
    selector: '#board',
    bootstrap: bsClass,
    position: {
      of: '#board',
      top: pTop,
      left: pLeft
    },
    size: {
      width: sWidth,
      height: sHeight
    },
    title: panelTitle,
    content: '<iframe id="frame-'+panelId+'" src="'+frameLink+'"></iframe>',
    draggable: {
      containment: "parent"
    },
    callback: function (panel) {
      // Refresh
      $('#jsPanel-'+panelId+' .jsPanel-btn-reload').click(function () {
        refreshTile(panelId);
      });
      // Edit
      $('#jsPanel-'+panelId+' .jsPanel-btn-edit').click(function () {
        if( $('#jsPanel-'+panelId+' .jsPanel-hdr-toolbar:visible').length == 0 ) {
          $('#jsPanel-'+panelId+' .jsPanel-hdr-toolbar').show();
        } else {
          $('#jsPanel-'+panelId+' .jsPanel-hdr-toolbar').hide();
        }
      });
      editTile(panelId);
      // minHeight + hash refresh on drag&drop
      $( '.jsPanel-hdr.panel-heading' )
        .mousedown(function() {
          minHeight = parseInt($( '#board' ).height())+parseInt($(this).parent().height())+20;
          $( '#board' ).css( 'min-height', minHeight );
        })
        .mouseup(function() {
          minHeight = 0;
          jQuery('.jsPanel').each(function() {
            minHeight = Math.max(
              minHeight,
              parseInt($(this).css('top').replace('px',''))+parseInt($(this).height())
            );
          })
          minHeight += 20;
          $( '#board' ).css( 'min-height', minHeight );
          makePermalink();
        });
      // hash refresh on close
      $('#jsPanel-'+panelId+' .jsPanel-btn-close').click(function () {
        makePermalink();
      });
      // hash refresh on resize
      $( '.jsPanel .ui-resizable-handle' ).mouseup(function() {
        makePermalink();
      });
    }
  });
  f$('#addWeb').modal('hide'); $('#addWeb input').val('');
  makePermalink();
};

function urlFilter(url) {
  // If it's an iframe code instead of an url
  // the src attribute is extracted
  if(url.indexOf('<iframe') > -1) {
    tmpIframe = document.createElement('div');
    $(tmpIframe).html(url);
    cleanUrl = $(tmpIframe).find('iframe').attr('src');
  } else {
    cleanUrl = url;
  }

  // If there is no https://
  // we need to clean the begining
  cleanUrl = cleanUrl.replace(/^(http\:\/\/)/,'https://')
                     .replace(/^(https?\/\/)/,'https://')
                     .replace(/^(\:\/\/)/,'https://')
                     .replace(/^(\/\/)/,'https://');
  if(cleanUrl.substr(0, 4) != 'http' && cleanUrl.substr(-5, 5) != '.html') {
    cleanUrl = 'https://'+cleanUrl;
  }

  // Replace Youtube link by Youtube player
  cleanUrl = cleanUrl.replace(/(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=))([\w\-]{10,12})\b[?=&\w]*(?!['"][^<>]*>|<\/a>)/ig, 'https://www.youtube.com/embed/$1');

  return cleanUrl;
}

// Function to add a tile on the board
// and send it with TogetherJS if needed
function sendTile(pTop,pLeft,sWidth,sHeight,url) {
  newTile(pTop,pLeft,sWidth,sHeight,url);
  if (TogetherJS.running && !MaestroJSConfig_disableTile) {
    TogetherJS.send({type: "newTile", pTop: pTop, pLeft: pLeft, sWidth: sWidth, sHeight: sHeight, url: url, author: currentUser() });
  }
}

function editTile(id) {
  $('#editTile-'+id).submit(function( event ) {
    $('#frame-'+id).attr('src', urlFilter($('#url-'+id).val()) );
    $('#url-'+id).attr('value', $('#frame-'+id).attr('src')); // write URL in DOM (needed in refresh case)
    $('#jsPanel-'+id+' .jsPanel-hdr-toolbar').hide();
    if (TogetherJS.running && !MaestroJSConfig_disableTile) {
      TogetherJS.send({type: "editTile", id: id, url: encodeURIComponent($('#frame-'+id).attr('src'))});
    }
    makePermalink();
    event.preventDefault();
  });
}

function refreshTile(id) {
  $('#frame-'+id).attr('src', $('#url-'+id).val());
}

/**----------------------------- TogetherJS -------------------------**/
TogetherJS.hub.on('newTile', function (msg) {
  id = randomName();
  $('body').append(
    '<div class="modal fade" id="dialog-'+id+'" tabindex="-1" role="dialog" aria-labelledby="dialog'+id+'Label" aria-hidden="true">'+
      '<div class="modal-dialog"><div class="modal-content">'+
        '<div class="modal-header">'+
          '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Fermer</span></button>'+
          '<h1 id="dialog'+id+'Label">Ajouter une fenêtre</h1>'+
        '</div>'+
        '<form class="form" id="dialog'+id+'Form" action="#">'+
          '<div class="modal-body">'+
            '<p><b>'+msg.author+'</b> vient d’ajouter à sa session cette page web :</p>'+
            '<div class="form-group"><input class="form-control" readonly="" type="text" value="'+
              decodeURIComponent((msg.url+'').replace(/\+/g, '%20'))+'"/></div>'+
            '<p>Voulez-vous également l’ajouter à la votre ?</p>'+
          '</div>'+
          '<div class="modal-footer">'+
            '<a href="#" class="btn btn-danger" data-dismiss="modal">Refuser</a>'+
            '<a href="#" class="btn btn-success" data-dismiss="modal">Accepter</a>'+
          '</div>'+
        '</form>'+
      '</div></div>'+
    '</div>'
  );
  f$('#dialog-'+id).modal('show');
  $('#dialog-'+id+' .btn-success').on('click', function() {
    newTile(msg.pTop, msg.pLeft, msg.sWidth, msg.sHeight, msg.url);

  });
  f$('#dialog-'+id).on('hidden.bs.modal', function (e) {
    f$(this).remove();
  })
});

TogetherJS.hub.on('editTile', function (msg) {
  url = decodeURIComponent((msg.url+'').replace(/\+/g, '%20'))
  $('#url-'+msg.id).val(url).attr('value',url);
  $('#frame-'+msg.id).attr('src', url);
});

TogetherJS.hub.on('sync', function (msg) {
  id = randomName();
  $('body').append(
    '<div class="modal fade" id="dialog-'+id+'" tabindex="-1" role="dialog" aria-labelledby="dialog'+id+'Label" aria-hidden="true">'+
      '<div class="modal-dialog"><div class="modal-content">'+
        '<div class="modal-header">'+
          '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Fermer</span></button>'+
          '<h1 id="dialog'+id+'Label">Synchroniser la session</h1>'+
        '</div>'+
        '<form class="form" id="dialog'+id+'Form" action="#">'+
          '<div class="modal-body">'+
            '<p><b>'+msg.author+'</b> vous propose de mettre à jour votre session à partir de la sienne.</p>'+
            '<p>Voulez-vous effectuer cette synchronisation ?</p>'+
          '</div>'+
          '<div class="modal-footer">'+
            '<a href="#" class="btn btn-danger" data-dismiss="modal">Refuser</a>'+
            '<a href="#" class="btn btn-success" data-dismiss="modal">Accepter</a>'+
          '</div>'+
        '</form>'+
      '</div></div>'+
    '</div>'
  );
  f$('#dialog-'+id).modal('show');
  $('#dialog-'+id+' .btn-success').on('click', function() {
    location.href = decodeURIComponent((msg.url));
    location.reload();
  });
});

function currentUser() {
  if( localStorage.getItem('togetherjs.settings.name') != undefined ) {
    author = localStorage.getItem('togetherjs.settings.name').replace(/"/g,'');
  } else if( localStorage.getItem('togetherjs.settings.defaultName') != undefined )  {
    author = localStorage.getItem('togetherjs.settings.defaultName').replace(/"/g,'');
  } else {
    author = 'Anonymous';
  }
  return author;
}

/**----------------------------- DOM Ready ---------------------------**/
$(document).ready(function() {
  // Redirection vers l'accueil si le Maestro est en panne d'inspiration :)
  if(HASH == '') {
    window.location.href = window.location.href.replace('p/', '').replace('#','');
  }

/**------------------------- Share & Link -------------------------- **/
  /**
   * Update Maestro permalinks values in the "Share" modal
   * - collabOptions are saved in a base36 number (= 2 last chars of the TogetherJS ShareID )
   **/
  function updatePermalink() {

    url = makePermalink();

    // Permalink + Shortlink
    $('#pLHref').attr('href',url);
    $('#pLVal').val(url);
    if( $('#sLVal').val() ) {
      $('#sLVal').parent().parent('.form-group').show();
      $('#sLHref').attr('href', $('#sLVal').val());
    };

    // Collaborative Permalink + Shortlink
    var collabConfig = 0;
    if( $('#collabChat').is(':checked') ) { collabConfig += 1 }
    if( $('#collabAudio').is(':checked') ) { collabConfig += 2 }
    if( $('#collabCursor').is(':checked') ) { collabConfig += Math.pow(2, 2) }
    if( $('#collabClic').is(':checked') ) { collabConfig += Math.pow(2, 3) }
    if( $('#collabTile').is(':checked') ) { collabConfig += Math.pow(2, 4) }
    if( $('#collabPush').is(':checked') ) { collabConfig += Math.pow(2, 5) }

    collabConfig = collabConfig.toString(36);
    if (collabConfig.length == 1)  { collabConfig = '0'+collabConfig }

    $('#pLCoHref').attr('href',url+'&togetherjs='+tonality+collabConfig);
    $('#pLCoVal').val(url+'&togetherjs='+tonality+collabConfig);
    if( $('#sLCoVal').val() ) {
      $('#sLCoVal').parent().parent('.form-group').show();
      $('#sLCoHref').attr('href', $('#sLCoVal').val());
    };

  }

  /**
   * Create short link using Frama.link API
   * value is set in a readonly input
   **/
  function makeShortlink(collab) {

    shortLink = tonality+'-'+concerto+'-'+(Math.floor((Date.now() - 1481497200000) / 1000).toString(36)); // 1481497200000 = 2016-12-12 00:00:00
    fullLink = (collab) ? makePermalink(true)+$('#pLCoVal').val().substr(-22,22) : makePermalink(true) ;
    id = (collab) ? 'Co' : '';

    $.ajax({
      method: "POST",
      url: lstu,
      dataType: 'json',
      data: { 'lsturl': fullLink, 'lsturl-custom': shortLink, 'format': 'json' }
    })
    .done(function( msg ) {
      if(msg.success) {
        $('#sL'+id+'Val').val('https://frama.link/'+shortLink).parent().parent('.form-group').show();
        $('#sL'+id+'Href').attr('href', $('#sL'+id+'Val').val());
        $('#sL'+id+'Btn').hide();
      } else {
        alert('Erreur :'+msg.msg);
      }
    });

  }

  /**
   * and…
   *
   * ACTION !
   **/
  $('title').append(' - '+concerto);

  // Load Tiles on the board
  for(i = 0; i < movement.length; i++) {

    if ( tile[i][0] != ''
      && tile[i][1] != ''
      && tile[i][2] != ''
      && tile[i][3] != ''
      && tile[i][4] != '' ) {

      newTile(parseInt(tile[i][0]),parseInt(tile[i][1]),parseInt(tile[i][2]),parseInt(tile[i][3]),tile[i][4]);

    }

  }

  // Share modal
  updatePermalink();
  $('a[href="#share"]').click( function() {
    updatePermalink();
    $('#sLBtn, #sLCoBtn').show();
  });

  // Sync
  $('#btnSync').click( function() {
    makePermalink();
    TogetherJS.send({type: 'sync', url: encodeURIComponent(location.href), author: currentUser() });
    return false;
  });

  // Shortlink buttons
  $('#sLBtn').on('click', function() { makeShortlink(); });
  $('#sLCoBtn').on('click', function() { makeShortlink(true); });

  // Collab settings
  // Preset from current config
  $('#collabClic').prop('checked', !TogetherJSConfig_dontShowClicks);
  $('#collabCursor').prop('checked', !MaestroJSConfig_dontShowCursor);
  $('#collabAudio').prop('checked', !TogetherJSConfig_disableWebRTC);
  $('#collabChat').prop('checked', !MaestroJSConfig_disableChat);
  $('#collabTile').prop('checked', !MaestroJSConfig_disableTile);
  $('#collabPush').prop('checked', !MaestroJSConfig_disablePush);

  $('#collabOptions input').on('change', function() {
    updatePermalink();
  });

  $('#collabClic').on('change', function() {
    if( $('#collabClic').is(':checked') ) {
      $('#collabCursor').prop('checked', true);
      $('label[for="collabCursor"]').addClass('text-muted');
    } else {
      $('label[for="collabCursor"]').removeClass('text-muted');
    };
  });

  $('label[for="collabCursor"]').on('click', function() {
    if( $(this).hasClass('text-muted') ) {
      return false
    }
  });

//-------------------------- Navbar buttons --------------------------//
/**------------------------- Sizes & Positions --------------------- **/
  $('#minimizeAll').on('click', function() {
    $('.jsPanel .jsPanel-btn-min').trigger('click');
    $('#expandAll').show(); $('#minimizeAll').hide();

    return false;
  });

  $('#expandAll').on('click', function() {
    $('.jsPanel .jsPanel-btn-norm').trigger('click');
    $('#expandAll').hide(); $('#minimizeAll').show();
    return false;
  });

  // Dirty hack to replace tile at the good left position
  // after a normalize();
  setInterval(function(){
    $('main .jsPanel').each(function() {
      id = f$(this).attr('id').replace('jsPanel-','');
      if(
        panels[ id ] != undefined
        && $('#jsPanel-'+id).css('left') != panels[ id ].option.position.left
        && !$('#jsPanel-'+id).hasClass('ui-draggable-dragging')
      ) {
        panels[id].reposition({
          top: panels[ id ].option.position.top,
          left: panels[ id ].option.position.left
        });
      }
    });
  }, 500);

  $('#grid table button').on('click', function() {
    $('.jsPanel .jsPanel-btn-norm').trigger('click');

    sWidth = window.innerWidth-(20*2);
    sHeight = window.innerHeight-$('#header').height()-$('#framanav').height()-40;

    switch ($('#grid table button').index(this)) {

      // Colonnes
      case 0:
        pTop = sHeight+20; pLeft = 20;
        $('.jsPanel').each(function(i) {
          panels[ ($(this).attr('id').replace('jsPanel-','')) ]
            .resize(sWidth+'px',sHeight+'px')
            .reposition({top: i*pTop, left: pLeft});
        });
      break;
      case 1:
        cols = 2;
        sWidth = ( sWidth - (cols-1)*20 ) / cols;
        pTop = sHeight+20; pLeft = sWidth+20;
        $('.jsPanel').each(function(i) {
          panels[ ($(this).attr('id').replace('jsPanel-','')) ]
            .resize(sWidth+'px',sHeight+'px')
            .reposition({top: Math.floor(i / cols)*pTop, left: 20+(i % cols)*pLeft });
        });
      break;
      case 2:
        cols = 3;
        sWidth = ( sWidth - (cols-1)*20 ) / cols;
        pTop = sHeight+15; pLeft = sWidth+20;
        $('.jsPanel').each(function(i) {
          panels[ ($(this).attr('id').replace('jsPanel-','')) ]
            .resize(sWidth+'px',sHeight+'px')
            .reposition({top: Math.floor(i / cols)*pTop, left: 20+(i % cols)*pLeft });
        });
      break;

      // Moaïque
      case 3:
        sHeight = Math.max(360, sHeight/2);
        pTop = sHeight+20; pLeft = 20;
        $('.jsPanel').each(function(i) {
          panels[ ($(this).attr('id').replace('jsPanel-','')) ]
            .resize(sWidth+'px',sHeight+'px')
            .reposition({top: i*pTop, left: pLeft});
        });
      break;
      case 4:
        cols = 2;
        sWidth = ( sWidth - (cols-1)*20 ) / cols;
        sHeight = Math.max(360, sHeight/2);
        pTop = sHeight+20; pLeft = sWidth+20;
        $('.jsPanel').each(function(i) {
          panels[ ($(this).attr('id').replace('jsPanel-','')) ]
            .resize(sWidth+'px',sHeight+'px')
            .reposition({top: Math.floor(i / cols)*pTop, left: 20+(i % cols)*pLeft });
        });
      break;
      case 5:
        cols = 3;
        sWidth = ( sWidth - (cols-1)*20 ) / cols;
        sHeight = Math.max(360, sHeight/2);
        pTop = sHeight+20; pLeft = sWidth+20;
        $('.jsPanel').each(function(i) {
          panels[ ($(this).attr('id').replace('jsPanel-','')) ]
            .resize(sWidth+'px',sHeight+'px')
            .reposition({top: Math.floor(i / cols)*pTop, left: 20+(i % cols)*pLeft });
        });
      break;

      // Autre
      case 6:
        cols = 3;
        sWidth = ( sWidth - (cols-1)*20 ) / cols;
        sHeight = Math.max(360, sHeight/2);
        pTop = sHeight+20; pLeft = sWidth+20;
        $('.jsPanel').each(function(i) {

          prevWidth = (i > 0) ? $('.jsPanel').eq(i-1).width() : sWidth;
          nextWidth = (i % 4 == 1 || i % 4 == 2 ) ? 2*sWidth+20 : sWidth;
          pLeft = (i > 0 && i % 2 == 1) ? 40+prevWidth : 20;

          panels[ ($(this).attr('id').replace('jsPanel-','')) ]
            .resize(nextWidth+'px',sHeight+'px')
            .reposition({top: Math.floor(i / (cols-1))*pTop, left: pLeft });
        });
      break;
      case 7:
        cols = 2;
        sHeight = Math.max(360, sHeight/2);
        pTop = sHeight+20; pLeft = sWidth+20;
        j = 0;
        $('.jsPanel').each(function(i) {
          nextWidth = ( i % 3 != 0 ) ? ( sWidth - (cols-1)*20 ) / cols : sWidth;
          prevTop = (i > 0) ? $('.jsPanel').eq(i-1).css('top').replace('px','') : pTop;
          pLeft =  ( i % 3 != 2 ) ? 20 : nextWidth+40;
          nextTop = j*pTop;
          panels[ ($(this).attr('id').replace('jsPanel-','')) ]
            .resize(nextWidth+'px',sHeight+'px')
            .reposition({top: nextTop, left: pLeft });
          j++; if ( i % 3 == 1 ) j = j-1;
        });
      break;
      case 8:
        cols = 3;
        sWidth = ( sWidth - (cols-1)*20 ) / cols;
        sHeight = Math.max(360, sHeight/2);
        pTop = sHeight+20; pLeft = sWidth+20;
        j = 0;
        $('.jsPanel').each(function(i) {

          nextTop = j*pTop;
          prevWidth = (i > 0) ? $('.jsPanel').eq(i-1).width() : sWidth;
          if (i % 3 == 0 ) {
            nextHeight = 2*sHeight+20;
            nextWidth = 2*sWidth;
            pLeft = 20;
          } else {
            nextWidth = sWidth;
            nextHeight = sHeight;
            pLeft = 2*sWidth+20+20;
            j++;
          }

          panels[ ($(this).attr('id').replace('jsPanel-','')) ]
            .resize(nextWidth+'px',nextHeight+'px')
            .reposition({top: nextTop, left: pLeft });
        });
      break;
    }
    makePermalink();
  });

/**------------------------- Communication ------------------------- **/
  $('#btnChat').on('click', function() {
    $('#togetherjs-chat-button').trigger('click'); return false;
  });

  $('#btnAudio').on('click', function() {
    $('#togetherjs-audio-button').trigger('click'); return false;
  });

  $('#btnTalk').on('click', function() {
    var _sizes = Array([640,360], [854,480], [1280,720]);

    if( $('iframe[src="'+jitsi+'/'+tonality+concerto+'"]').length > 0 ) {
      alert('Il y a déjà une visio conférence active sur la page');
    } else {
      sendTile(0,20,_sizes[1][0],_sizes[1][1],jitsi+'/'+tonality+concerto);
    }
    return false;
  });

  $('#addIRCForm').submit(function( event ) {
    var _sizes = Array([640,360], [854,480], [1280,720]);

    kiwi += ( !$('#ircChannel').val() ) ? 'irc.freenode.net' : $('#ircServer').val();
    kiwi += ( /^#.+/.test($('#ircChannel').val()) ) ? '/'+$('#ircChannel').val() : '';

    if( $('iframe[src="'+kiwi+'"]').length > 0 ) {
      alert('Il y a déjà un salon IRC à cette adresse actif sur la page');
    } else {
      sendTile(0,20,_sizes[1][0],_sizes[1][1],encodeURIComponent( kiwi ));
    }
    $(this).parent().parent().removeClass('open');
    event.preventDefault();
  });

/**------------------------- Apps -----------------------------------**/
  addFramaservices = $('#framaServices');
  for (var k in framaServices) {
    addFramaservices.append(
      '<a class="btn btn-default col-xs-4" href="'+framaServices[k].u+tonality+concerto+'" id="FS'+k+'">'+
        '<i class="fa fa-fw fa-lg '+framaServices[k].i+'" aria-hidden="true"></i><br>'+framaServices[k].t+
      '</a>'
    );
  }

  $('#framaServices a').on('click', function() {
    var _sizes = Array([640,360], [854,480], [1280,720]);
    url = $(this).attr('href')+($('iframe[src^="'+$(this).attr('href')+'"]').length+1);
    sendTile(0,20,_sizes[1][0],_sizes[1][1],encodeURIComponent(url));
    return false;
  });

  $('#waybackMachine').on('click', function() {
    url = $('#addWeb input').val().replace('https://web.archive.org/web/','');
    $('#addWeb input').val('https://web.archive.org/web/'+url);
  });

  $('#addWebForm').submit(function( event ) {
    var _sizes = Array([640,360], [854,480], [1280,720]);
    sendTile(0,20,_sizes[1][0],_sizes[1][1],encodeURIComponent( urlFilter($('#addWeb input').val()) ));
    $('.modal-backdrop').remove();
    event.preventDefault();
  });
});